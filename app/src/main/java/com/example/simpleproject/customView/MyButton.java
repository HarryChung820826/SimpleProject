package com.example.simpleproject.customView;

import android.content.Context;
import android.view.MotionEvent;

import com.example.simpleproject.utils.LogUtil;

public class MyButton extends androidx.appcompat.widget.AppCompatButton {
    public MyButton(Context context) {
        super(context);
    }

    /**分發**/
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        LogUtil.getInstance(this).printLog("dispatchTouchEvent");
        return super.dispatchTouchEvent(ev);
    }

    /**消費**/
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        LogUtil.getInstance(this).printLog("onTouchEvent");
        return super.onTouchEvent(event);
    }
}
