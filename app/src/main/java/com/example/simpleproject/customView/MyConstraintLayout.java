package com.example.simpleproject.customView;

import android.content.Context;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.simpleproject.utils.LogUtil;

public class MyConstraintLayout extends ConstraintLayout {
    public MyConstraintLayout(@NonNull Context context) {
        super(context);
    }

    /**分發**/
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        LogUtil.getInstance(this).printLog("dispatchTouchEvent");
        return super.dispatchTouchEvent(ev);
    }

    /**攔截**/
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        LogUtil.getInstance(this).printLog("onInterceptTouchEvent");
        return super.onInterceptTouchEvent(ev);
    }

    /**消費**/
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        LogUtil.getInstance(this).printLog("onTouchEvent");
        return super.onTouchEvent(event);
    }
}
