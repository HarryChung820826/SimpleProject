package com.example.simpleproject.utils;

import android.util.Log;

public class LogUtil {

    private static LogUtil instance;
    private static String key = "";

    public static LogUtil getInstance() {
        return getInstance(null);
    }

    public static LogUtil getInstance(Object object) {
        if (instance == null) {
            instance = new LogUtil();
        }

        if (object != null) {
            key = object.getClass().getSimpleName();
        } else {
            key = "";
        }

        return instance;
    }

    public void printLog(String message) {
        Log.i(key , message);
    }
}
