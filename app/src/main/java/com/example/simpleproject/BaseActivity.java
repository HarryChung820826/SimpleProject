package com.example.simpleproject;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.simpleproject.utils.LogUtil;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtil.getInstance(this).printLog("onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
        LogUtil.getInstance(this).printLog("onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogUtil.getInstance(this).printLog("onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        LogUtil.getInstance(this).printLog("onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        LogUtil.getInstance(this).printLog("onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LogUtil.getInstance(this).printLog("onDestroy");
    }
}
