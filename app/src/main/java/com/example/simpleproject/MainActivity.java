package com.example.simpleproject;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.simpleproject.listActivity.SimpleListActivity;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((Button) findViewById(R.id.button_goto_second_page)).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        gotoListActivity();
    }

    private void gotoListActivity() {
        Intent intent = new Intent(this, SimpleListActivity.class);
        startActivity(intent);
    }
}