package com.example.simpleproject.listActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.simpleproject.R;

import java.util.ArrayList;
import java.util.Arrays;

public class SimpleAdapter extends RecyclerView.Adapter<SimpleViewHolder> {

    private ArrayList<String> data = (ArrayList<String>) Arrays.asList("item A", "item B", "item C", "item D", "item E");

    /**
     * 透過 ViewHolder 定義 Item UI
     * **/
    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_simple, parent, false);
        return new SimpleViewHolder(view);
    }

    /**
     * Item 呈現於畫面上時會呼叫此Function，用於定義 Item 內的資訊
     * **/
    @Override
    public void onBindViewHolder(@NonNull SimpleViewHolder holder, int position) {
        holder.setContent(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
