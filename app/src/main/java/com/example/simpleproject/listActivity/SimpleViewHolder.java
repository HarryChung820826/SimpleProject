package com.example.simpleproject.listActivity;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.simpleproject.R;

public class SimpleViewHolder extends RecyclerView.ViewHolder {

    private TextView textView;

    public SimpleViewHolder(@NonNull View itemView) {
        super(itemView);
        textView = itemView.findViewById(R.id.item_content);
    }

    public void setContent(String content) {
        textView.setText(content);
    }
}
